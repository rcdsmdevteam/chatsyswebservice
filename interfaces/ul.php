<?php
header('Content-Type: text/html; charset=utf-8');
try{
    include_once('../scripts/fcore.php');

    if(     isset($_GET['user'])
        AND isset($_GET['pass'])
        AND (       isset($_POST['text'])
            OR  isset($_GET['text']))
        AND isset($_GET['dest'])
        AND !isset($_GET['promo'])){

        if(isset($_POST['text'])){
            $text = $_POST['text'];
        }else{
            $text = $_GET['text'];
        }
        upload($_GET['user'], $_GET['pass'], $text, $_GET['dest']);
    }elseif(    isset($_GET['user'])
        AND isset($_GET['pass'])
        AND (       isset($_POST['text'])
            OR  isset($_GET['text']))
        AND !isset($_GET['dest'])
        AND isset($_GET['promo'])){

        if(isset($_POST['text'])){
            $text = $_POST['text'];
        }else{
            $text = $_GET['text'];
        }
        upload($_GET['user'], $_GET['pass'], $text, null, true);
    }elseif(    isset($_GET['user'])
        AND isset($_GET['pass'])
        AND (       isset($_POST['text'])
            OR  isset($_GET['text']))
        AND !isset($_GET['dest'])
        AND !isset($_GET['promo'])){

        if(isset($_POST['text'])){
            $text = $_POST['text'];
        }else{
            $text = $_GET['text'];
        }
        upload($_GET['user'], $_GET['pass'], $text);
    }else{
        throw new CustomException('Erreur;Requête HTTP incorrecte !');
    }
}catch (CustomException $e){
    echo $e->getMessage();
}catch (Exception $e){
    echo 'Erreur;Une erreur inattendue s\'est produite lors de l\'éxécution de le requête !';
}