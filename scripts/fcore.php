<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 11/5/14
 * Time: 11:40 AM
 */
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Paris');

define('DEV', false);
if(DEV){
    define('SQLCHATSYS', 'rcdsm_chatsys_dev_');
}else{
    define('SQLCHATSYS', 'rcdsm_chatsys_');
}

class CustomException extends Exception{
}

function encodeOutMessages($text){
	return utf8_decode($text);
}

function db_connect(){
    try{
        //$connexion = new PDO('mysql:host=localhost;dbname=lopma_tsdi', 'root', '');
        $connexion = new PDO('mysql:host=lopma_tsdi.mysql.db;dbname=lopma_tsdi', 'lopma_tsdi', 'Rcdsm000');
        //$connexion = new PDO('mysql:host=217.128.252.165;dbname=rcdsm_chatsys', 'rcdsm_chatsys', 'Rcdsm000');
        //$connexion = new PDO('mysql:host=localhost;dbname=rcdsm2014_chatsys', 'root', 'pf68');
        if(!DEV){
            $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        }else{
            $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        }
    }catch (PDOException $e){
        throw new CustomException('Erreur;Echec de la connexion à la base de donnée !');
    }
    return $connexion;
}

function upload($user, $pass, $text, $dest=null, $promo=false){
    try{
        $connection = db_connect();
        $check = userCheck($user, $pass, true);
        if($check != 0){
            $promoid = 0;
            $desttest = true;
            $dests = array();
            if(     $dest != null
                AND !$promo){
                $dests = explode(',', $dest);
                $cpt = 0;
                while($cpt < count($dests)){
                    $destsData[$cpt] = userCheck($dests[$cpt]);
                    if($destsData[$cpt] == 0){
                        $desttest = false;
                    }
                    $cpt++;
                }
                if(!$desttest){
                    throw new CustomException('Erreur;Un ou plusieurs destinataires n\'ont pas puent être trouvés !');
                }
            }elseif(    $dest == null
                AND $promo){
                $data = $connection->query('SELECT fk_promo FROM '.SQLCHATSYS.'users WHERE id = \''.$check.'\'')->fetch(PDO::FETCH_ASSOC);
                $promoid = $data['fk_promo'];
            }elseif(    $dest != null
                AND $promo){
                throw new CustomException('Erreur;Impossible d\'ajouter un message qui possède à la fois une référence de promotion et un ou plusieurs destinataires !');
            }
            $req = $connection->prepare('INSERT INTO '.SQLCHATSYS.'messages(fk_auteur, contenu, datePost, fk_promo) VALUES (?, ?, ?, ?);');
            $tab = array($check, $text, date("Y-m-d H:i:s"), $promoid);
            $req->execute($tab);
            $idmsg = $connection->lastInsertId();
            if(count($dests) > 0 AND $desttest){
                foreach($destsData as $destinataire){
                    $req = $connection->prepare('INSERT INTO '.SQLCHATSYS.'msgdest(fk_dest, fk_message) VALUES (?, ?);');
                    $tab = array($destinataire, $idmsg);
                    $req->execute($tab);
                }
            }
            echo encodeOutMessages('OK;'.$idmsg.';Message envoyé avec succès !');
        }else{
            throw new CustomException('Erreur;Identifiant ou MDP incorrect !');
        }
    }catch (CustomException $e){
        echo encodeOutMessages($e->getMessage());
    }
}

function download($user, $pass, $all=false){
    try{
        $connection = db_connect();
        $check = userCheck($user,$pass, true);
        if($check != 0){
            $sql = '  SELECT    msgs.id,
                                auteur.pseudo AS auteur,
                                msgs.datePost,
                                msgs.contenu,
                                msgs.fk_promo AS promo

                      FROM      '.SQLCHATSYS.'messages msgs,
                                '.SQLCHATSYS.'users auteur

                      WHERE     msgs.fk_auteur = auteur.id
                            AND (   msgs.fk_auteur = \''.$check.'\'
                                OR  msgs.id IN (SELECT fk_message FROM '.SQLCHATSYS.'msgdest WHERE fk_dest = \''.$check.'\'
                                OR  msgs.fk_promo = (SELECT fk_promo FROM '.SQLCHATSYS.'users WHERE id = \''.$check.'\'))
                                OR  msgs.id IN (
                                    SELECT id FROM '.SQLCHATSYS.'messages WHERE fk_promo = 0 AND id NOT IN (
                                        SELECT fk_message FROM '.SQLCHATSYS.'msgdest GROUP BY fk_message))
                                )';

            if(!$all){
                $sql .= ' AND msgs.datePost > \''.date("Y-m-d H:i:s", time()-60).'\'';
            }
            $sql.= ' ORDER BY datePost DESC';
            if($all){$sql .= ' LIMIT 30';}
            $msgs = $connection->query($sql.';');
            echo '{"messages":[';
            $stop = $msgs->rowCount();
            $cpt = 0;
            while ($msg = $msgs->fetch(PDO::FETCH_ASSOC)){
                $cpt++;
                $destsCount = $connection->query('SELECT count(*) AS count FROM '.SQLCHATSYS.'msgdest msgdest, '.SQLCHATSYS.'users users WHERE msgdest.fk_dest = users.id AND msgdest.fk_message = \''.$msg['id'].'\'')->fetch(PDO::FETCH_ASSOC);
                $destinataires = '';
                if($destsCount['count'] > 0){
                    $dests = $connection->query('SELECT users.pseudo AS dest FROM '.SQLCHATSYS.'msgdest msgdest, '.SQLCHATSYS.'users users WHERE msgdest.fk_dest = users.id AND msgdest.fk_message = \''.$msg['id'].'\'');
                    while ($dest = $dests->fetch(PDO::FETCH_ASSOC)){
                        if($destinataires != ''){
                            $destinataires .= ',';
                        }
                        $destinataires .= $dest['dest'];
                    }
                }
                if($msg['promo'] != 0){
                    $data = $connection->query('SELECT libelle FROM '.SQLCHATSYS.'promo WHERE id = \''.$msg['promo'].'\'')->fetch(PDO::FETCH_ASSOC);
                    $promo = $data['libelle'];
                }else{
                    $promo = '';
                }

                echo '{"id":"'.$msg['id'].'","promo":"'.$promo.'","auteur":"'.$msg['auteur'].'","date":"'.$msg['datePost'].'","contenu":"'.str_replace('"', '\"', str_replace('\\', '\\\\', stripcslashes($msg['contenu']))).'","destinataire":"'.$destinataires.'"}';
                if($cpt != $stop){
                    echo ',';
                }
            }
            echo ']}';
        }else{
            throw new CustomException('Erreur;Identifiant ou MDP incorrect !');
        }
    }catch (CustomException $e){
        echo encodeOutMessages($e->getMessage());
    }
}

function info($user = null){
    try{
        $connection = db_connect();
        $sql = 'SELECT pseudo, mail, phone, couleur, nom, prenom, libelle FROM '.SQLCHATSYS.'users, '.SQLCHATSYS.'promo WHERE fk_promo = '.SQLCHATSYS.'promo.id';
        if ($user != null) {
            $check = userCheck($user);
            if ($check != 0) {
                $sql = $sql . ' AND pseudo = \'' . $user . '\'';
            } else {
                throw new CustomException('Erreur;Utilisateur introuvable !');
            }
        }
        $list = $connection->query($sql . ';');
        echo '{"infos":[';
        $stop = $list->rowCount();
        $cpt = 0;
        while ($ligne = $list->fetch(PDO::FETCH_ASSOC)) {
            $cpt++;

            echo '{"promo":"'.$ligne['libelle'].'","nom":"'.$ligne['nom'].'","prenom":"'.$ligne['prenom'].'","pseudo":"'.$ligne['pseudo'].'","mail":"'.$ligne['mail'].'","tel":"'.$ligne['phone'].'","couleur":"'.$ligne['couleur'].'"}';
            if ($cpt != $stop) {
                echo ',';
            }
        }
        echo ']}';
    }catch (CustomException $e){
        echo encodeOutMessages($e->getMessage());
    }
}

function majProfile($user, $pass, $newpass, $couleur){
    try{
        $connection = db_connect();
        $check = userCheck($user,$pass, true);
        if($check != 0) {
            $list = $connection->query('SELECT * FROM '.SQLCHATSYS.'users WHERE id = \''.$check.'\';');
            $data = $list->fetch(PDO::FETCH_ASSOC);

            $new['couleur'] = $couleur != null ? $couleur : $data['couleur'];
            $new['newpass'] = $newpass != null ? $newpass : $data['pass'];

            $req = $connection->prepare('UPDATE '.SQLCHATSYS.'users SET pass = ?, couleur = ? WHERE id = \''.$check.'\';');
            $tab = array($new['newpass'], $new['couleur']);
            $req->execute($tab);

            echo encodeOutMessages('OK;'.$check.';Modification(s) effectuée(s) avec succès !');
        }else{
            throw new CustomException('Erreur;Identifiant ou MDP incorrect !');
        }
    }catch (CustomException $e){
        echo encodeOutMessages($e->getMessage());
    }
}

function userCheck($user, $pass=null, $doPass=false){
    $sql = 'SELECT id, count(*) AS count FROM '.SQLCHATSYS.'users WHERE pseudo = \''.$user.'\'';
    if($doPass){
        $sql .= ' AND pass = \''.$pass.'\'';
    }
    $data = db_connect()->query($sql.';')->fetch(PDO::FETCH_ASSOC);
    return $data['count'] == 1 ? $data['id'] : 0;
}