<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 11/5/14
 * Time: 4:09 PM
 */
header('Content-Type: text/html; charset=utf-8');
try{
	include_once('../scripts/fcore.php');

	if(     isset($_GET['user'])
		AND isset($_GET['pass'])
		AND (   isset($_GET['newpass'])
            OR	isset($_GET['couleur'])
		)){
		if(isset($_GET['newpass'])){
			$newpass = $_GET['newpass'];
		}else{
			$newpass = null;
		}
		if(isset($_GET['couleur'])){
			$couleur = $_GET['couleur'];
		}else{
			$couleur = null;
		}
		majProfile($_GET['user'], $_GET['pass'], $newpass, $couleur);
	}else{
		throw new CustomException('Erreur;Requête HTTP incorrecte !');
	}
}catch (CustomException $e){
	echo $e->getMessage();
}catch (Exception $e){
    echo 'Erreur;Une erreur inattendue s\'est produite lors de l\'éxécution de le requête !';
}