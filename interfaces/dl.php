<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 11/5/14
 * Time: 4:09 PM
 */
header('Content-Type: text/html; charset=utf-8');
try{
    include_once('../scripts/fcore.php');
    if(     isset($_GET['all'])
        AND isset($_GET['user'])
        AND isset($_GET['pass'])
    ){
        download($_GET['user'], $_GET['pass'], true);
    }elseif(    isset($_GET['user'])
        AND isset($_GET['pass'])
    ){
        download($_GET['user'], $_GET['pass']);
    }else{
        throw new CustomException('Erreur;Requête HTTP incorrecte !');
    }
}catch (CustomException $e){
    echo $e->getMessage();
}catch (Exception $e){
    echo 'Erreur;Une erreur inattendue s\'est produite lors de l\'éxécution de le requête !';
}